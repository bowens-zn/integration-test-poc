import aiohttp
import asyncio


async def api_get(endpoint, token, assertion=True):
    status, result = None, None
    async with aiohttp.ClientSession(trust_env=True, auth=aiohttp.BasicAuth(token, "")) as session:
        async with session.get(endpoint) as response:
            status = response.status
            result = await response.text()
    if status is not 200 and assertion:
        raise Exception(
            f'request_issue_page returned status: {status}, result: {result}')
    else:
        print(f'Successful request_issue_page: {result}')
    return result

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    # search only for critical vulnerabilities
    res = loop.run_until_complete(api_get(
        'http://mock:8500/api/issues/search?projectKeys=cybric.ApIWNmRhTZyMEQkJ2gWhJw.webgoat&ps=500&p=1&types=VULNERABILITY&statuses=OPEN,REOPENED,CONFIRMED&severities=BLOCKER', 'c29tZXBhc3N3b3Jk'))

    # assert severity
    assert '"severity":"BLOCKER"' in res
    assert '"severity":"CRITICAL"' not in res
    assert '"severity":"MAJOR"' not in res  # not found at all in real api
    assert '"severity":"MINOR"' not in res
    assert '"severity":"INFO"' not in res  # not found at all in real api

    # assert issue type
    assert '"type":"VULNERABILITY"' in res
    assert '"type":"BUG"' not in res
    assert '"type":"CODE_SMELL"' not in res
    assert 'metric' not in res
    assert 'measures' not in res
