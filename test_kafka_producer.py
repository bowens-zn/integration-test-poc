from aiokafka import AIOKafkaProducer
from aiokafka.errors import KafkaError
import asyncio

loop = asyncio.get_event_loop()


async def send_one():
    producer = AIOKafkaProducer(
        loop=loop, bootstrap_servers='kafka:9092')
    # Get cluster layout and initial topic/partition leadership information
    await producer.start()
    try:
        # Produce message
        # TODO -  do we use plaintext or bytes?
        resp = await producer.send_and_wait("events", b"Events message")
        print(resp)
        resp = await producer.send_and_wait("sonarqube", b"Sonarqube message")
        print(resp)

    except KafkaError as err:
        print('Kafka producer error: ' + err)
        raise err
    finally:
        # Wait for all pending messages to be delivered or expire.
        await producer.stop()

loop.run_until_complete(send_one())
