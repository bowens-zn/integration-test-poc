from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou, BucketAlreadyExists)
import os


minioClient = Minio('minio:9000', access_key='minio', secret_key='minio123', secure=False)

try:
       minioClient.make_bucket("test-bucket", location="default")
except BucketAlreadyOwnedByYou as err:
       pass
except BucketAlreadyExists as err:
       pass
except ResponseError as err:
       raise

assert minioClient.bucket_exists("test-bucket")

try:
       with open('test-file', 'rb') as test_file:
              file_stat = os.stat('test-file')
              minioClient.put_object("test-bucket", "test-object", test_file, file_stat.st_size)
              data = minioClient.get_object("test-bucket", "test-object")

except ResponseError as err:
       print(err)

assert data.read().decode("ascii") == "test content"